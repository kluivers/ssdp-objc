//
//  JKUPDSocket.h
//  ssdp
//
//  Created by Joris Kluivers on 11/6/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SSDPBrowser;

@protocol SSDPBrowserDelegate <NSObject>
- (void) browser:(SSDPBrowser *)browser foundService:(NSDictionary *)service;
@end

@interface SSDPBrowser : NSObject

@property(nonatomic, weak) id<SSDPBrowserDelegate> delegate;

- (id) initWithDelegate:(id<SSDPBrowserDelegate>)delegate;

- (void) startBrowsing;
- (void) stopBrowsing;

@end
