//
//  JKUPDSocket.m
//  ssdp
//
//  Created by Joris Kluivers on 11/6/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>

#import "SSDPBrowser.h"

@implementation SSDPBrowser
{
	dispatch_queue_t _browseQueue;
	BOOL _active;
}

- (id) initWithDelegate:(id<SSDPBrowserDelegate>)delegate
{
	self = [super init];
	
	if (self)
	{
		_delegate = delegate;
		_browseQueue = dispatch_queue_create("nl.tarento.ssdp_queue", DISPATCH_QUEUE_SERIAL);
	}
	
	return self;
}

- (void) startBrowsing
{
	_active = YES;
	dispatch_async(_browseQueue, ^{
		[self _browse];
	});
}

- (void) stopBrowsing
{
	_active = NO;
}

- (void) _browse
{
	NSString *ip = @"239.255.255.250";
	NSUInteger port = 1900;
	
	NSString *msg = @"M-SEARCH * HTTP/1.1\n\
Host: 239.255.255.250:1900\n\
Man: ssdp:discover\n\
Mx: 3\n\
ST: \"ssdp:all\"";
	
	int sock;
	struct sockaddr_in destination;
	struct sockaddr_in myaddr;
	size_t echolen;
	int broadcast = 1;
	
	if (!msg || !ip)
	{
		NSLog(@"Message and/or ip address is null");
		return;
	}
	
	/* Create the UDP socket */
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
	{
		NSLog(@"Failed to create socket");
		return;
	}
	
	/* Construct the server sockaddr_in structure */
	memset(&destination, 0, sizeof(destination));
	
	/* Clear struct */
	destination.sin_family = AF_INET;
	
	/* Internet/IP */
	destination.sin_addr.s_addr = inet_addr([ip UTF8String]);
	
	/* IP address */
	destination.sin_port = htons(port);
	
	/* server port */
	setsockopt(sock,
			   IPPROTO_IP,
			   IP_MULTICAST_IF,
			   &destination,
			   sizeof(destination));
	const char *cmsg = [msg UTF8String];
	echolen = strlen(cmsg);
	
	// this call is what allows broadcast packets to be sent:
	if (setsockopt(sock,
				   SOL_SOCKET,
				   SO_BROADCAST,
				   &broadcast,
				   sizeof broadcast) == -1)
	{
		perror("setsockopt (SO_BROADCAST)");
		exit(1);
	}
	if (sendto(sock,
			   cmsg,
			   echolen,
			   0,
			   (struct sockaddr *) &destination,
			   sizeof(destination)) != echolen)
	{
		printf("Mismatch in number of sent bytes\n");
		return;
	}
	else
	{
		NSLog(@"-> Tx: %@", msg);
	}
	
	myaddr.sin_family = PF_INET;
	myaddr.sin_addr.s_addr = INADDR_ANY;
	myaddr.sin_port = htons(port);
	memset(&(myaddr.sin_zero), '\0', 8);
	
	/*if (bind(sock, (struct sockaddr *)&myaddr, sizeof(struct sockaddr)) == -1) {
		NSLog(@"Error binding socket");
	}*/
	
	fd_set readfs;
	struct sockaddr addr;
	socklen_t fromlen;
	char buf[512];
	size_t len = 0;
	
	FD_ZERO(&readfs);
	FD_SET(sock, &readfs);
	int sel, n = sock + 1;
	struct timeval timeout;
	timeout.tv_sec = 2;
	timeout.tv_usec = 500000;
	
	//while (() > 0) {
	while (YES) {
		sel = select(n, &readfs, NULL, NULL, &timeout);
		
		if (!_active) {
			NSLog(@"Stop browsing");
			
			// TODO: clean up socket
			
			break;
		}
		
		if (sel == 0) {
			// timeout: try again
		} else if (sel == -1) {
			// error: try again
		} else {
			if (FD_ISSET(sock, &readfs)) {
				len = recvfrom(sock, &buf, sizeof(buf), 0, &addr, &fromlen);
				buf[len] = '\0';
				
				NSString *dataString = [[NSString alloc] initWithBytes:buf length:len encoding:NSUTF8StringEncoding];
				NSDictionary *serviceDict = [self serviceDictFromString:dataString];
				
				dispatch_async(dispatch_get_main_queue(), ^() {
					if ([self.delegate respondsToSelector:@selector(browser:foundService:)]) {
						[self.delegate browser:self foundService:serviceDict];
					}
				});
			}
		}
	}
}

- (NSDictionary *) serviceDictFromString:(NSString *)string {
	NSArray *lines = [string componentsSeparatedByString:@"\n"];
	
	NSMutableDictionary *serviceDict = [NSMutableDictionary dictionary];
	
	for (NSString *line in lines) {
		NSString *trimmed = [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		
		NSUInteger colonLocation = [trimmed rangeOfString:@":"].location;
		if (colonLocation == NSNotFound) {
			continue;
		}
		
		NSString *key = [trimmed substringToIndex:colonLocation];
		NSString *value = nil;
		
		if (colonLocation + 1 < [trimmed length]) {
			value = [trimmed substringWithRange:NSMakeRange(colonLocation + 1, [trimmed length] - (colonLocation + 1))];
		}
		
		if (key && value) {
			serviceDict[key] = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		}
	}
	
	return serviceDict;
}

@end
