//
//  JKAppDelegate.h
//  ssdp
//
//  Created by Joris Kluivers on 11/6/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface JKAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
