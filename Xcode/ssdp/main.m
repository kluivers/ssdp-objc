//
//  main.m
//  ssdp
//
//  Created by Joris Kluivers on 11/6/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
