//
//  JKAppDelegate.m
//  ssdp
//
//  Created by Joris Kluivers on 11/6/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import "JKAppDelegate.h"

#import "SSDPBrowser.h"

@interface JKAppDelegate () <SSDPBrowserDelegate>
@end

@implementation JKAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// Insert code here to initialize your application
	
	SSDPBrowser *browser = [[SSDPBrowser alloc] initWithDelegate:self];
	[browser startBrowsing];
	
	// Delay execution of my block for 10 seconds.
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
		[browser stopBrowsing];
	});
}

- (void) browser:(SSDPBrowser *)browser foundService:(NSDictionary *)service
{
	static NSMutableDictionary *devices = nil;
	if (!devices) {
		devices = [NSMutableDictionary dictionary];
	}
	
	//NSLog(@"Found service: %@", service);
	if ([service[@"ST"] isEqualToString:@"upnp:rootdevice"]) {
		NSString *USN = service[@"USN"];
		if ([devices objectForKey:USN]) {
			return;
		}
		
		devices[USN] = service;
		
		NSLog(@"Root device!. Info at: %@", service[@"LOCATION"]);
	}
}
@end
