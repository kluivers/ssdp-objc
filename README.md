# Simple Service Discovery Protocol browser for Objective-C

A simplistic SSDP browser initially written to detect a Philips Hue basestation on the local network.

[more info on my blog](http://joris.kluivers.nl/hue/)
